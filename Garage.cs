﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_27
{
    public class Garage : IGarage
    {
        private List<Car> cars = new List<Car>();
        private List<string> carTypes = new List<string>();

        public Garage(List<string> carTypes)
        {
            this.carTypes = carTypes;
        }

        public void AddCar (Car car)
        {
            if (cars.Contains(car) == true)
            {
                throw new CarAlreadyHereException("THis car in the list");
            }
            else if (car.TotalLost == true)
            {
                throw new WeDoNotFixTotalLostException("No total lost here!!");
            }
            else if (string.IsNullOrEmpty(car.Brand) == true)
            {
                throw new CarNullException("Be serious!");
            }
            else if (carTypes.Contains(car.Brand) == false)
            {
                throw new WrongGarageException("This " + car.Brand + " not in my garage!");
            }
            else if (car.NeedsRepair == false)
            {
                throw new RepairMismatchException("The is OK");
            }
            else
            {
                cars.Add(car);
            }
        }
        public void TakeOutCar (Car car)
        {
            if (string.IsNullOrEmpty(car.Brand) == true)
            {
                throw new CarNullException("Be serious!");
            }
            else if (cars.Contains(car) == false)
            {
                throw new CarNotInGarageException("Not here!!");
            }
            else if (car.NeedsRepair == true)
            {
                throw new CarNotReadyException("Still working on it");
            }
            else
            {
                cars.Remove(car);
            }
        }
        public void FixCar (Car car)
        {
            if (string.IsNullOrEmpty(car.Brand) == true)
            {
                throw new CarNullException("Be serious!");
            }
            else if (cars.Contains(car) == false)
            {
                throw new CarNotInGarageException("Not here!!");
            }
            else if (car.NeedsRepair == false)
            {
                throw new RepairMismatchException();
            }
            else
            {
                car.NeedsRepair = false;
            }
        }
    }
}

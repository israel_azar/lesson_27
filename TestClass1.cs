﻿using System;
using System.Collections.Generic;
using Lesson_27;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarGarageTest
{
    [TestClass]
    public class TestClass1
    {
        Car honda = new Car("Civic", false, true);
        Car Seat = new Car("Ibiza", false, true);
        Car Toyota = new Car("Corolla", false, true);
        List<string> carTypes;
    [TestInitialize]
        public void TestIntelizer()
        {
            carTypes = new List<string>
            {
                honda.Brand,
                Seat.Brand,
                Toyota.Brand
            };
        }
        [TestMethod]
        public void AddCarTest()
        {
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
        }
        [TestMethod]
        [ExpectedException (typeof(CarAlreadyHereException))]
        public void AddCarTestWithExceptions1()
        {
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
            garage.AddCar(honda);
        }
        [TestMethod]
        [ExpectedException (typeof(WeDoNotFixTotalLostException))]
        public void AddCArWithExceptions2()
        {
            Seat = new Car("Ibiza", true, true);
            Garage garage = new Garage(carTypes);
            garage.AddCar(Seat);
        }
        [TestMethod]
        [ExpectedException (typeof(WrongGarageException))]
        public void AddCarWithExceptions3()
        {
            Car Mazda = new Car("Cx5", false, true);
            Garage garage = new Garage(carTypes);
            garage.AddCar(Mazda);
        }
        [TestMethod]
        [ExpectedException (typeof(CarNullException))]
        public void AddCarWithExceptions4()
        {
            Toyota = new Car(null, false, true);
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
        }
        [TestMethod]
        [ExpectedException (typeof(RepairMismatchException))]
        public void AddCarWithExceptions5()
        {
            honda = new Car("Civic", false, false);
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
        }
        [TestMethod]
        public void TakeOutCarTest()
        {
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
            Seat.NeedsRepair = false;
            garage.TakeOutCar(Seat);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void TakeOutCarTestWithExceptions1()
        {
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
            Seat = new Car(null, false, true);
            garage.TakeOutCar(Seat);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void TakeOutCarTestWithExceptions2()
        {
            Car mazda = new Car("Cx5", false, true);
            Garage garage = new Garage(carTypes);
            garage.TakeOutCar(mazda);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotReadyException))]
        public void TakeOutCarTestWithExceptions3()
        {
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
            garage.TakeOutCar(Toyota);
        }
        [TestMethod]
        public void FixCarTest()
        {
            Garage garage = new Garage(carTypes);
            garage.AddCar(honda);
            garage.AddCar(Seat);
            garage.AddCar(Toyota);
            garage.FixCar(honda);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void FixCarTestWithExceptions1()
        {
            Garage garage = new Garage(carTypes);
            Seat = new Car(null, false, true);
            garage.FixCar(Seat);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void FixCarTestWithExceptions2()
        {
            Garage garage = new Garage(carTypes);
            Car mazda = new Car("Cx5", false, true);
            garage.FixCar(mazda);
        }
        [TestMethod]
        [ExpectedException(typeof(RepairMismatchException))]
        public void FixCarTestWithExceptions3()
        {
            Garage garage = new Garage(carTypes);
            honda = new Car("Civic", false, false);
            garage.AddCar(honda);
            garage.FixCar(honda);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_27
{
    interface IGarage
    {
        void AddCar(Car car);
        void TakeOutCar(Car car);
        void FixCar(Car car);
    }
}
